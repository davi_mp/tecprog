#include <stdio.h>
#include<stdlib.h>
#include <math.h>
#include "xwc.h"
#include<time.h>
#include"pos.h"
#define G 0.0000000000667
#define width 1280
#define height 720
#define w_planeta 264
#define h_planeta 120

/* 
 * O ep 1 calcula as posições e o efeito da gravidade em condições mais próximas da realidade, logo
 * precisamos adaptar os números para caber numa tela 1280x720 (tamanho escolhido para
 * a tela). Para isso escolhemos dividir as coordenadas por 100000. Além disso trasladamos o plano do ep1 para o ep2
 * porque o ponto (0, 0) do ep1 não corresponde ao (0, 0) da tela do jogo. Disso, tivemos que somar metade do tamanho da tela
 * nas respectivas coordenadas.
 */
int transformax(double x){
  /*coluna*/
  int x1;
  x1 = (int)(x/100000.0) + width/2;
  return x1;
}

/*Analoga a funçao transformax, sendo que para o y*/
int transformay(double y){
  /*linha*/
  int y1;
  y1 = (int)(y/100000.0) + height/2;
  return y1;
}



int main(int ac, char **av)
{
  int collision = 0;
  corpo * corpos;
  WINDOW *w;
  double ti, tf, tj;
  MASK p1, planeta, mao, seta, error, start, end;
  PIC P1, PLANETA, nave1, nave2;
  PIC Error, Start, End;
  printf("Digite o tempo de cada simulacao em segundos: ");
  scanf("%lf", &tempoSimulacao);
  corpos = lerArquivo();
  ti = clock();
  tf = clock();

  puts("Iniciando o jogo e abrindo uma janela");
  w = InitGraph(width, height, "Janelao");

  start = NewMask(w, width, height);
  Start = ReadPic(w, "start.xpm", start);
  SetMask(w, start);
  PutPic(w, Start, 0,0, width, height, 0, 0);
  ti = clock();
  tf = ti;
  while((tf - ti) < 3*CLOCKS_PER_SEC){
    tf = clock();
  }
  

  p1 = NewMask(w, width, height);
  P1 = ReadPic(w, "windows.xpm", p1);
  SetMask(w, p1);
  PutPic(w, P1, 0,0, width, height, 0, 0);



  planeta = NewMask(w, w_planeta, h_planeta);
  PLANETA = ReadPic(w, "index.xpm", planeta);
  SetMask(w, planeta);
  PutPic(w, PLANETA, 0, 0, w_planeta, h_planeta, (width/2) - (w_planeta/2), (height/2)-(h_planeta/2));

  mao = NewMask(w, 40, 40);
  nave1 = ReadPic(w, "mao.xpm", mao);

  seta = NewMask(w, 40, 40);
  nave2 = ReadPic(w, "seta.xpm", seta);



  int x1, y1, x2, y2;
  x1 = x2 = y1 = y2 = 0;
  tj = clock();
  while(tj < tempoTotal*CLOCKS_PER_SEC){

    atualiza();

    SetMask(w, p1);
    PutPic(w, P1, 0,0, width, height, 0, 0);
    SetMask(w, planeta);
    PutPic(w, PLANETA, 0, 0, w_planeta, h_planeta, (width/2)-(w_planeta/2), (height/2)-(h_planeta/2));


    x1 = transformax(corpos[1].pos_x);
    y1 = transformay(corpos[1].pos_y);
    /*verifica colisão da nave1 com o "planeta*//*
    if(x1 + 15 > width/2 - w_planeta/2 && x1 + 15 < width/2 + w_planeta/2 && y1 + 15 > height/2 - h_planeta/2 && y1 + 15 < height/2 + h_planeta/2){
      collision = 1;
      break;
    }*/
    printf("x1: %d   y1:  %d\n", x1, y1);
    x2 = transformax(corpos[2].pos_x);
    y2 = transformay(corpos[2].pos_y);
    /*verifica colisão da nanve2 com o "planeta"*//*
    if(x2 + 15 > width/2 - w_planeta/2 && x2 + 15 < width/2 + w_planeta/2 && y2 + 15 > height/2 - h_planeta/2 && y2 + 15 < height/2 + h_planeta/2){
      collision = 1;
       break;
    }*/
    /*verifica colisão entre as naves*/
    if(abs(x1-x2) < 20 && abs(y1-y2) < 20){collision = 1; break;}

    printf("x2: %d   y2:  %d\n\n", x2, y2);
    SetMask(w, mao);
    PutPic(w, nave1, 0, 0, 40, 40, x1-20, y1-20);
    SetMask(w, seta);
    PutPic(w, nave2, 0, 0, 40, 40, x2-20, y2-20);
    /*
    puts("Atualizando");
    puts("Tecle <enter>"); getchar();*/
    while((tf - ti) < tempoSimulacao*CLOCKS_PER_SEC){
         tf = clock();
    }

    ti = tf;
    tj = clock();
  }
  if(collision){
    error = NewMask(w, width, height);
    Error = ReadPic(w, "error.xpm", error);
    SetMask(w, error);
    PutPic(w, Error, 0,0, width, height, 0, 0);
  }
  else{
    end = NewMask(w, width, height);
    End = ReadPic(w, "end.xpm", end);
    SetMask(w, end);
    PutPic(w, End, 0,0, width, height, 0, 0);
  }



  ti = clock();
  tf = ti;
  while((tf - ti) < 2*CLOCKS_PER_SEC){
  tf = clock();
  }
  UnSetMask(mao);
  UnSetMask(seta);
  UnSetMask(planeta);
  UnSetMask(p1);
  UnSetMask(start);
  if(collision) UnSetMask(error);
  else UnSetMask(end);
  free(corpos);
  corpos = NULL;

  CloseGraph();
  return 0;
}
