/*
EP1 de TecProg
Davi de Menezes Pereira     nusp: 11221988
Lucas Irineu R. Guimarães   nusp: 11221713
Luca Barcelos               nusp: 11221672
*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#define G 0.0000000000667

double tamanhoDoPlaneta, massaDoPlaneta, tempoSimulacao, numeroDeProjeteis, tempoDeVida, tempoTotal;

/* struct para guardar os corpos no espaço (caso seja o projetil, tera o nome '*') e no caso do planeta, nao tera nome */
typedef struct {
    char nome[15];
    double massa;
    double pos_x;
    double pos_y;
    double vel_x;
    double vel_y;
} corpo;

corpo * corpos;

void lerArquivo(){
    int i = 0;
    FILE * arquivo_de_entrada;
    char nome_arquivo[80];
    corpos = malloc(sizeof(corpo)*1000);
    printf("Digite o nome do arquivo de dados: ");
    scanf("%s", nome_arquivo);
    arquivo_de_entrada = fopen(nome_arquivo, "r");
    for(i = 0; !feof(arquivo_de_entrada); i++){
        if(!i){
            fscanf(arquivo_de_entrada, "%lf %lf %lf", &tamanhoDoPlaneta, &massaDoPlaneta, &tempoTotal);
            corpos[0].massa = massaDoPlaneta;
            corpos[0].vel_y = corpos[0].vel_x = 0;
            corpos[0].pos_y = corpos[0].pos_x = 0;
        }
        else if((i > 0)&&(i < 3)){
            fscanf(arquivo_de_entrada, "%s %lf %lf %lf %lf %lf", (corpos[i].nome), &(corpos[i].massa), &(corpos[i].pos_x), &(corpos[i].pos_y), &(corpos[i].vel_x), &(corpos[i].vel_y));
        }
        else if(i == 3){
            fscanf(arquivo_de_entrada, "%lf %lf", &numeroDeProjeteis, &tempoDeVida);
        }
        else {
            fscanf(arquivo_de_entrada, "%lf %lf %lf %lf %lf", &(corpos[(i - 1)].massa), &(corpos[(i - 1)].pos_x), &(corpos[(i - 1)].pos_y), &(corpos[(i - 1)].vel_x), &(corpos[(i - 1)].vel_y));
            corpos[(i-1)].nome[0] = '*';
        }
    }

}

/* Função para atualizar a posição do corpo i*/
void calculaPosicao(int i){
  corpos[i].pos_x += corpos[i].vel_x * tempoSimulacao;
  corpos[i].pos_y += corpos[i].vel_y * tempoSimulacao;
}


/*Função para calular a força entre dois corpos apenas (e devolve o resultado dela no corpo i)*/
void calculaForca(double * fx, double * fy, int i, int j){
  double dx, dy, d, ax, ay;
  double seno, cosseno;
  dx = corpos[j].pos_x - corpos[i].pos_x;
  dy = corpos[j].pos_y - corpos[i].pos_y;
  if((1e-6 < dx || dx < -1e-6)&&(1e-6 < dy || dy < -1e-6)){  
    d = sqrt( dx * dx + dy * dy );
    seno = dy/d;
    cosseno = dx/d;
    ax = (G * corpos[j].massa * cosseno)/ (d*d);
    ay = (G * corpos[j].massa * seno) / (d*d);
    (*fx) += ax * corpos[i].massa;   
    (*fy) += ay * corpos[i].massa;
  }
}


/*  Essa função calcula a forca resultante na nave i e qual vai ser sua
    velocidade na proxima iteracao */
void calculaVelocidade(int i){
    int j = 0;
    /* variaveis para ajudar no calculo da forca decomposta nos eixos x e y*/
    double fxr = 0, fyr = 0;
    for(j = 0; j < numeroDeProjeteis + 3; j++)
        /* nao calcula força dele nele mesmo */
        if(j != i) calculaForca(&fxr, &fyr, i, j);

    corpos[i].vel_x += fxr * tempoSimulacao / corpos[i].massa;
    corpos[i].vel_y += fyr * tempoSimulacao / corpos[i].massa;
}

/**/
void atualiza(){
  int i;
  for(i = 0; i < numeroDeProjeteis + 3; i++){
    calculaVelocidade(i);
    calculaPosicao(i);
  } 
}

int main(){
    double ti, tf;
    printf("Digite o tempo de cada simulacao em segundos: ");
    scanf("%lf", &tempoSimulacao);
    lerArquivo();
    ti = clock();
    tf = clock();
    while(tf < tempoTotal* CLOCKS_PER_SEC){
        for(int i = 0; i < numeroDeProjeteis + 3; i++){
            if(i == 0){
                printf("PLANETA: %.3lf (raio) %.3lf (massa)", tamanhoDoPlaneta, massaDoPlaneta);
            }
            else if(i < 3){
                printf("nave: %s | massa: %.2lf | pos(x): %.2lf | pos(y): %.2lf | vel(x): %.2lf | vel(y): %.2lf", (corpos[i].nome), (corpos[i].massa), (corpos[i].pos_x), (corpos[i].pos_y), (corpos[i].vel_x), (corpos[i].vel_y));
            }
            else{
                printf("(projetil) massa: %.2lf | pos(x): %.2lf | pos(y): %.2lf | vel(x): %.2lf | vel(y): %.2lf", (corpos[i].massa), (corpos[i].pos_x), (corpos[i].pos_y), (corpos[i].vel_x), (corpos[i].vel_y));
            }
            printf("\n");
        }
        printf("\n");
        atualiza();
        while((tf - ti) < tempoSimulacao*CLOCKS_PER_SEC){
            tf = clock();
        }
        ti = tf;
    }

    return 0;
}
