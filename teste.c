#include <stdio.h>
#include<stdlib.h>
#include <math.h>
#include "xwc.h"
#include<time.h>
#define G 0.0000000000667
#define width 600
#define height 600

double tamanhoDoPlaneta, massaDoPlaneta, tempoSimulacao, numeroDeProjeteis, tempoDeVida, tempoTotal;

/* struct para guardar os corpos no espaço (caso seja o projetil, tera o nome '*') e no caso do planeta, nao tera nome */
typedef struct {
    char nome[15];
    double massa;
    double pos_x;
    double pos_y;
    double vel_x;
    double vel_y;
} corpo;

corpo * corpos;

void lerArquivo(){
    int i = 0;
    FILE * arquivo_de_entrada;
    char nome_arquivo[80];
    corpos = malloc(sizeof(corpo)*100);
    printf("Digite o nome do arquivo de dados: ");
    scanf("%s", nome_arquivo);
    arquivo_de_entrada = fopen(nome_arquivo, "r");
    for(i = 0; !feof(arquivo_de_entrada); i++){
        if(!i){
            fscanf(arquivo_de_entrada, "%lf %lf %lf", &tamanhoDoPlaneta, &massaDoPlaneta, &tempoTotal);
            corpos[0].massa = massaDoPlaneta;
            corpos[0].vel_y = corpos[0].vel_x = 0;
            corpos[0].pos_y = corpos[0].pos_x = 0;
        }
        else if((i > 0)&&(i < 3)){
            fscanf(arquivo_de_entrada, "%s %lf %lf %lf %lf %lf", (corpos[i].nome), &(corpos[i].massa), &(corpos[i].pos_x), &(corpos[i].pos_y), &(corpos[i].vel_x), &(corpos[i].vel_y));
        }
        else if(i == 3){
            fscanf(arquivo_de_entrada, "%lf %lf", &numeroDeProjeteis, &tempoDeVida);
        }
        else {
            fscanf(arquivo_de_entrada, "%lf %lf %lf %lf %lf", &(corpos[(i - 1)].massa), &(corpos[(i - 1)].pos_x), &(corpos[(i - 1)].pos_y), &(corpos[(i - 1)].vel_x), &(corpos[(i - 1)].vel_y));
            corpos[(i-1)].nome[0] = '*';
        }
    }

}

/* Função para atualizar a posição do corpo i - alterada em relação ao ep 1*/
void calculaPosicao(int i){
  /*coluna*/
  corpos[i].pos_x += (corpos[i].vel_x * tempoSimulacao);
  while(corpos[i].pos_x > (width*50000.0)) corpos[i].pos_x -= width*100000.0;
  while(corpos[i].pos_x < -(width*50000.0)) corpos[i].pos_x += width*100000.0;

  /*linha*/
  corpos[i].pos_y += (corpos[i].vel_y * tempoSimulacao);
  while(corpos[i].pos_y > (height*50000.0)) corpos[i].pos_y -= height*100000.0;
  while(corpos[i].pos_y < -(height*50000.0)) corpos[i].pos_y += height*100000.0;

}


/*Função para calular a força entre dois corpos apenas (e devolve o resultado dela no corpo i)*/
void calculaForca(double * fx, double * fy, int i, int j){
  double dx, dy, d, ax, ay;
  double seno, cosseno;
  dx = corpos[j].pos_x - corpos[i].pos_x;
  dy = corpos[j].pos_y - corpos[i].pos_y;
  if((dx  < 10000 && dx > -10000)&&(-10000 < dy && dy < 10000)) return;
    d = sqrt( dx * dx + dy * dy );
    seno = dy/d;
    cosseno = dx/d;
    ax = (G * corpos[j].massa * cosseno)/ (d*d);
    ay = (G * corpos[j].massa * seno) / (d*d);
    (*fx) += ax * corpos[i].massa;   
    (*fy) += ay * corpos[i].massa;
}


/*  Essa função calcula a forca resultante na nave i e qual vai ser sua
    velocidade na proxima iteracao */
void calculaVelocidade(int i){
    int j = 0;
    /* variaveis para ajudar no calculo da forca decomposta nos eixos x e y*/
    double fxr = 0, fyr = 0;
    for(j = 0; j < numeroDeProjeteis + 3; j++)
        /* nao calcula força dele nele mesmo */
        if(j != i) calculaForca(&fxr, &fyr, i, j);
    corpos[i].vel_x += (fxr * tempoSimulacao) / corpos[i].massa;
    corpos[i].vel_y += (fyr * tempoSimulacao) / corpos[i].massa;
}

/**/
void atualiza(){
  int i;
  for(i = 1; i < numeroDeProjeteis + 3; i++){
    calculaVelocidade(i);
  }

  for(i = 1; i < numeroDeProjeteis + 3; i++){
    calculaPosicao(i);
  }
}


/*adicionado do ep2 (fora o modificado na funçao calcula posicao))*/
int transformax(double x){
  /*coluna*/
  int x1;
  x1 = (int)(x/100000.0) + width/2;
  return x1;
}

int transformay(double y){
  /*linha*/
  int y1;
  y1 = (int)(y/100000.0) + height/2;
  return y1;
}

int main(int ac, char **av)
{
  WINDOW *w;
  double ti, tf;
  printf("Digite o tempo de cada simulacao em segundos: ");
  scanf("%lf", &tempoSimulacao);
  lerArquivo();
  ti = clock();
  tf = clock();

  puts("Iniciando o sistema gráfico e abrindo uma janela");
  w = InitGraph(width, height, "Janelao");



  int x1, y1, x2, y2;
  x1 = x2 = y1 = y2 = 0;
  while(tf < tempoTotal*CLOCKS_PER_SEC){
    WFillRect(w, x1, y1, 40, 40, WNamedColor("black"));
    WFillRect(w, x2, y2, 40, 40, WNamedColor("black"));
    atualiza();

    x1 = transformax(corpos[1].pos_x);
    y1 = transformay(corpos[1].pos_y);
    printf("x1: %d   y1:  %d\n", x1, y1);
    WFillRect(w, x1, y1, 40, 40, WNamedColor("red"));
  

    x2 = transformax(corpos[2].pos_x);
    y2 = transformay(corpos[2].pos_y);
    printf("x2: %d   y2:  %d\n\n", x2, y2);
    WFillRect(w, x2, y2, 40, 40, WNamedColor("blue"));
/*
    puts("Atualizando");
    puts("Tecle <enter>"); getchar();*/


    while((tf - ti) < tempoSimulacao*CLOCKS_PER_SEC){
         tf = clock();
    }
    
    ti = tf;
  }


  CloseGraph();
  return 0;
}


