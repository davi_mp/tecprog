#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#define MAX 50
#define G 0,0000000000667
#define tempoIteracao 0,1

double tamanhoDoPlaneta, massaDoPlaneta, tempoSimulacao, numeroDeProjeteis, tempoDeVida;

double forca[MAX][2]

typedef struct {
    char nome[50];
    double massa;
    double pos_x;
    double pos_y;
    double vel_x;
    double vel_y;
} corpo;

corpo * corpos;

void lerArquivo(){
    int i = 0;
    FILE * arquivo_de_entrada;
    char nome_arquivo[80];
    corpos = malloc(sizeof(corpo)*1000);
    printf("Digite o nome do arquivo de dados: ");
    scanf("%s", nome_arquivo);
    arquivo_de_entrada = fopen(nome_arquivo, "r");
    for(i = 0; !feof(arquivo_de_entrada); i++){
        if(!i){
            fscanf(arquivo_de_entrada, "%lf %lf %lf", &tamanhoDoPlaneta, &massaDoPlaneta, &tempoSimulacao);
        }
        else if((i > 0)&&(i < 3)){
            fscanf(arquivo_de_entrada, "%s %lf %lf %lf %lf %lf", (corpos[(i - 1)].nome), &(corpos[(i - 1)].massa), &(corpos[(i - 1)].pos_x), &(corpos[(i - 1)].pos_y), &(corpos[(i - 1)].vel_x), &(corpos[(i - 1)].vel_y));
        }
        else if(i == 3){
            fscanf(arquivo_de_entrada, "%lf %lf", &numeroDeProjeteis, &tempoDeVida);
        }
        else {
            fscanf(arquivo_de_entrada, "%lf %lf %lf %lf %lf", &(corpos[(i - 2)].massa), &(corpos[(i - 2)].pos_x), &(corpos[(i - 2)].pos_y), &(corpos[(i - 2)].vel_x), &(corpos[(i - 2)].vel_y));
            corpos[(i-2)].nome = "projetil";
        }
    }

}

double modulo (double i){
  if(i>0) return i;
  else return -i;
}


/*esta função muda a posicao das naves no momento i*/
double posicao (int i){
  corpos[i].pos_x += corpos[i].vel_x * tempoIteracao;
  corpos[i].pos_y += corpos[i].vel_y * tempoIteracao;
}

/*  Essa função calcula a força resultante da nave i e qual vai ser sua
    velocidade na proxima iteração */
void forcaResultante(int i){
    int j = 0;
    double d, dx, dy, a, fx=0, fy=0, fxr=0, fyr=0, fr;
    /*caso i represente uma nave*/
    if(corpos[i].nome!="projetil"){
      for(j = 0; j < numeroDeProjeteis + 2; j++){
        if(j>1 && j<numeroDeProjeteis+2){
          dx=corpos[j].pos_x - corpos[i].pos_x;
          dy=corpos[j].pos_y - corpos[i].pos_y;
          d=sqrt(modulo(dx * dx + dy * dy));
          a= G * corpos[j].massa/ (d*d);
          /*
          confere se ta certo:
          Fgravitacional= G * m[i] * m[j] / r²
          Fresultante= m[i] * a;
          a= G * m[j] / r²;
          */
          fx += a * dx * tempoIteracao * corpos[i].massa / d;
          fy += a * dy * corpos[i].massa/ d;
          /*
            projeção X e Y da força com que o corpo j atrai o corpo i;
          */
        }
        else{
          if(j==1){
            dx= - corpos[i].pos_x;
            dy= - corpos[i].pos_y;
            d=sqrt(modulo(dx * dx + dy * dy));
            a= G * massaDoPlaneta/ (d*d);
            fx += a * dx * corpos[i].massa / d;
            fy += a * dy * corpos[i].massa/ d;
          }
          if(j==0 && corpos[i].nome!= corpos[j].nome){
            dx=corpos[j].pos_x - corpos[i].pos_x;
            dy=corpos[j].pos_y - corpos[i].pos_y;
            d=sqrt(modulo(dx * dx + dy * dy));
            a= G * corpos[j].massa/ (d*d);
            fx += a * dx * corpos[i].massa / d;
            fy += a * dy * corpos[i].massa/ d;
          }
          if(j==0 && corpos[i].nome!= corpos[j+1].nome){
            dx=corpos[j+1].pos_x - corpos[i].pos_x;
            dy=corpos[j+1].pos_y - corpos[i].pos_y;
            d=sqrt(modulo(dx * dx + dy * dy));
            a= G * corpos[j+1].massa/ (d*d);
            fx += a * dx * corpos[i].massa / d;
            fy += a * dy * corpos[i].massa/ d;
          }
        }
        fxr+=fx;
        fyr+=fy;
      }
    }
    /*caso i represente um projetil*/
    else{
      for(j = 0; j < numeroDeProjeteis + 2; j++){
        if(j!=1 && j<numeroDeProjeteis+1){
          dx=corpos[j].pos_x - corpos[i].pos_x;
          dy=corpos[j].pos_y - corpos[i].pos_y;
          d=sqrt(modulo(dx * dx + dy * dy));
          a= G * corpos[j].massa/ (d*d);
          fx += a * dx * corpos[i].massa / d;
          fy += a * dy * corpos[i].massa/ d;
        }
        else {
            dx= - corpos[i].pos_x;
            dy= - corpos[i].pos_y;
            d=sqrt(modulo(dx * dx + dy * dy));
            a= G * massaDoPlaneta/ (d*d);
            fx += a * dx * corpos[i].massa / d;
            fy += a * dy * corpos[i].massa/ d;
      }
      fxr+=fx;
      fyr+=fy;
    }
    corpos[i].vel_x+= fxr * tempoIteracao / corpos[i].massa;
    corpos[i].vel_y+= fyr * tempoIteracao / corpos[i].massa;
  }
}

void atualiza(){
  int j;
  for (j=0; j<numeroDeProjeteis; j++)
    posicao(j);
  for (j=0; j<numeroDeProjeteis; j++)
    forcaResultante(j);
}

int main(){
    int ti, tf;
    lerArquivo();
    ti = clock()* CLOCKS_PER_SEC;
    tf = clock()* CLOCKS_PER_SEC;
    while(tf<tempoSimulacao){
        while((tf - ti) < 1){
            tf = clock()* CLOCKS_PER_SEC;
        }
        atualiza();
        tm=ti;
    }
    return 0;
}
