#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#define MAX 50
#define G 0,0000000000667

double tamanhoDoPlaneta, massaDoPlaneta, tempoSimulacao, numeroDeProjeteis, tempoDeVida;

double forca[MAX][2];

typedef struct {
    char nome[50];
    double massa;
    double pos_x;
    double pos_y;
    double vel_x;
    double vel_y;
} corpo;

corpo * corpos;

void lerArquivo(){
    int i = 0;
    FILE * arquivo_de_entrada;
    char nome_arquivo[80];
    corpos = malloc(sizeof(corpo)*1000);
    printf("Digite o nome do arquivo de dados: ");
    scanf("%s", nome_arquivo);
    arquivo_de_entrada = fopen(nome_arquivo, "r");
    for(i = 0; !feof(arquivo_de_entrada); i++){
        if(!i){
            fscanf(arquivo_de_entrada, "%lf %lf %lf", &tamanhoDoPlaneta, &massaDoPlaneta, &tempoSimulacao);
        }
        else if((i > 0)&&(i < 3)){
            fscanf(arquivo_de_entrada, "%s %lf %lf %lf %lf %lf", (corpos[(i - 1)].nome), &(corpos[(i - 1)].massa), &(corpos[(i - 1)].pos_x), &(corpos[(i - 1)].pos_y), &(corpos[(i - 1)].vel_x), &(corpos[(i - 1)].vel_y));
        }
        else if(i == 3){
            fscanf(arquivo_de_entrada, "%lf %lf", &numeroDeProjeteis, &tempoDeVida);
        }
        else {
            fscanf(arquivo_de_entrada, "%lf %lf %lf %lf %lf", &(corpos[(i - 2)].massa), &(corpos[(i - 2)].pos_x), &(corpos[(i - 2)].pos_y), &(corpos[(i - 2)].vel_x), &(corpos[(i - 2)].vel_y));
            corpos[(i-2)].nome[0] = '*';
        }
    }

}

double modulo(double i){
  if(i>0) return i;
  else return -i;
}

/* Função para atualizar a posição do corpo i*/
void calculaPosicao(int i){
  corpos[i].pos_x += corpos[i].vel_x * tempoSimulacao;
  corpos[i].pos_y += corpos[i].vel_y * tempoSimulacao;
}


/*Função para calular a força entre dois corpos apenas (e devolve o resultado dela no corpo i)*/
void calculaForca(double * fx, double * fy, int i, int j){
  double dx, dy, d, ax, ay;
  double seno, cosseno;
  dx =corpos[j].pos_x - corpos[i].pos_x;
  dy =corpos[j].pos_y - corpos[i].pos_y;
  d = sqrt(dx * dx + dy * dy);
  seno = dy/dx;
  cosseno = dx/dy;
  ax = G * corpos[j].massa * cosseno/ (d*d);
  ay = G * corpos[j].massa * seno/ (d*d);
  (*fx) += ax * corpos[i].massa;
  (*fy) += ay * corpos[i].massa;
}


/*  Essa função calcula a força resultante da nave i e qual vai ser sua
    velocidade na proxima iteração */
void forcaResultante(int i){
    int j = 0;
    double fxr = 0, fyr = 0;
    /*caso i represente uma nave*/
    for(j = 0; j < numeroDeProjeteis + 2; j++)
      if(j != i) calculaForca(&fxr, &fyr, i, j);
    
    corpos[i].vel_x += fxr * tempoSimulacao / corpos[i].massa;
    corpos[i].vel_y += fyr * tempoSimulacao / corpos[i].massa;
}



void atualiza(){
  int i;
  for(i = 1; i < numeroDeProjeteis + 2; i++){
    forcaResultante(i);
    calculaPosicao(i);
  }
}

int main(){
    int ti, tf, tm;
    lerArquivo();
    ti = clock()* CLOCKS_PER_SEC;
    tf = clock()* CLOCKS_PER_SEC;
    while(tf<tempoSimulacao){
        while((tf - ti) < 1){
            tf = clock()* CLOCKS_PER_SEC;
        }
        atualiza();
        tm=ti;
    }
    return 0;
}
